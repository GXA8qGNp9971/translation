# Sportvorschrift für das Heer
## A translation of the Wehrmacht Fitness Manual

### File Structure

---

* scan.pdf
  * The original scan of Sportvorschrift für das Heer
* transcription.pdf
  * A typed version of the original scan
* translation.txt
  * A translation of the transcription from german to english
* translation.md
  * A conversion of translation.txt into the markdown format

Missing Sections:

- [x] 10-14
- [x] 22-25 Needs to be redone
- [x] 39-44
- [ ] Page 45 Bullet C and Figure 37 Placeholder
- [x] 49-52
- [ ] Page 60 Line Missing at the top above the picture
- [x] 67-69
- [x] 84
- [x] 96-97
- [x] 98-105
- [ ] Page 111 Line at the top of the page is missing
- [ ] Page 115 Final Paragraph
- [ ] 116-126

### Editing Guide

---

* Only make an edit if a sentence can be made to sound better without changing its meaning.
* If a sentence sounds very off and you question its translation, open an issue about it.

### How to Contribute

---

To edit on GitLab:

1. Click on translation.txt
2. Click edit
3. Make your changes
3. When finished, send your changes as a merge request

To edit locally:

1. Make an account on GitLab
2. Fork this repository by clicking the fork button at the top of the page
3. Make sure git is installed to your computer, [https://git-scm.com](https://git-scm.com)
4. Open the command line and navigate to the folder you want to work from (`cd <working directory>`)
5. Clone your repository to your computer (`git clone <link to your fork>`)
6. Add this repo to your local repo so you can fetch new updates (`git remote add upstream https://gitlab.com/sportvorschrift-fur-das-heer/translation` then `git fetch upstream`)
7. Create a new branch (`git checkout -b <name of your branch, i.e. what you're changing>`)
8. Make your changes
9. Stage your changes (`git add .`)
10. Commit your changes (`git commit -m "<what you changed>"`)
12. Update from the upstream (`git pull upstream master`)
11. Push your changes to GitLab (`git push origin`)
12. Return to your GitLab repo and open a merge request with your branch to have your changes reviewed and added to the main repo

Do not include **<** and **>** in your commands.

### The Plan

---

#### Phase 1: Collection of Material - Complete

* Rip PNGs of Images
* Transcribe the text into an unaltered German version

#### Phase 2: Elaboration of Material

* Clean up/redraw PNGs
* Translate the German Text
* Design a cover for the book

#### Phase 3: Profit

* Put everything together into the best book /pol/ has ever produced
